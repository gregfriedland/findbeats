
static int NUM_BANDS = 3;
static int MAX_VALS = 10;
static int LED_WIDTH = 100;
static boolean PRINT = false;
static int NUM_VALS = 6;

// colors for the vals to plot
static color[] colors = {#FFFFFF,  #FF0000, #00FF00, #0000FF, #FFFF00, #00FFFF, #FF00FF};
 
 import processing.serial.*;
 
 Serial myPort;        // The serial port
 int xPos = LED_WIDTH;         // horizontal position of the graph
 int lastTime, loopCount;
 
 void setup () {
   // set the window size:
   size(1300, 600);        
   
   // List all the available serial ports
   println(Serial.list());
   myPort = new Serial(this, Serial.list()[4], 115200);
   // don't generate a serialEvent() unless you get a newline character:
   myPort.bufferUntil('\n');
   background(0);
   
   lastTime = millis();
   loopCount = 0;
 }
 
 void draw () {
   // everything happens in the serialEvent()
 }
 
 float[][] lastValPos = new float[NUM_BANDS][MAX_VALS];

 void serialEvent (Serial myPort) {
    int lf = 10;
    byte[] inBuffer = new byte[1000];
    int len = myPort.readBytesUntil(lf, inBuffer);
   if (keyPressed && key == ' ') return;
    
    if ((len-1) % NUM_BANDS != 0) {
      println("Invalid buffer size: " + len);
      return;
    }
    int numVals = (len - 1) / NUM_BANDS;
    if (numVals != NUM_VALS) {
      println("Too many values received.");
      return;
    }
    
    int[][] vals = new int[NUM_BANDS][numVals];
    
    if (inBuffer != null) { // don't forget about 10 at the end
      for (int b=0; b<NUM_BANDS; b++) {
        for (int v=0; v<numVals; v++) {
          vals[b][v] = int(inBuffer[b*numVals+v]);
          if (PRINT) print(" " + nf(vals[b][v], 3));
        }
      }
      if (PRINT) println("");
    } else {
      println("Invalid buffer state.");
      return;  
    }

   for (int b=0; b<NUM_BANDS; b++) {
     // plot energy color
     float y = height/NUM_BANDS*(b+1);
     float y2 = height/NUM_BANDS*b;
     
     for (int v=0; v<numVals; v++) {
       if (v == 0) {
         if (vals[b][v] > 0) {
           fill(colors[b]);
         } else {
           fill(#000000);
         }
         stroke(#000000);
         rect(0, y2, LED_WIDTH, height/NUM_BANDS); 
       }

       float valPos = map(vals[b][v], 11, 255, 0, height/NUM_BANDS - 20); 
       color col = (colors[v] & 0xffffff) | (255 << 24);  // adjust alpha
       stroke(col);   
       
       line(xPos, y - valPos, xPos, y-lastValPos[b][v]);
       lastValPos[b][v] = valPos;
     }
   }
   
   // at the edge of the screen, go back to the beginning:
   if (xPos >= width) {
     xPos = LED_WIDTH;
     background(0); 
   } else {
     // increment the horizontal position:
     xPos++;
   }
   
   loopCount++;
   if (millis() - lastTime > 10000) {
     float fps = float(loopCount) / (millis() - lastTime) * 1000;
     println(fps + " fps");
     lastTime = millis();
     loopCount = 0;
   }
 }
 
