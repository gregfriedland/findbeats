// This sketch flashes leds 13, 12, and 11 in time with the beat on the spectrum shield

#include <FindBeats.h>

#define ANALOG_PIN A0
#define RESET_PIN 5
#define STROBE_PIN 4

#define BEAT_HOLD_TIME 250    // how long to hold the beat for
#define BEAT_WAIT_TIME 350    // how many ms to wait before allowing another beat                                                                                          
#define SENSITIVITY 17        // how sensitive to be when looking for beats; higher is more sensitive
#define UPDATE_PERIOD 5       // the most often time interval that beats are updated: 5ms is a good number; 20 is too high; lower uses more RAM

// only find beats on 3 bands b/c it's already a lot of info and it speed things up
byte bandLeds[] = {0,13,0,11,0,9,0};
boolean bandsUsed[] = {0,1,0,1,0,1,0}; // which bands are in use
FindBeats findBeats(bandsUsed, UPDATE_PERIOD);

void setup() {
  Serial.begin(115200);
  findBeats.setBeatWaitTime(BEAT_WAIT_TIME);
  findBeats.setBeatHoldTime(BEAT_HOLD_TIME);
  findBeats.init(ANALOG_PIN, STROBE_PIN, RESET_PIN);
}

void loop() {
  findBeats.updateBeats(SENSITIVITY);

  for (byte b=0; b<NUM_BANDS; ++b) {
    if (bandsUsed[b] && findBeats.isBeat(b)) {
      byte intensity = findBeats.getBeatIntensity(b);
      analogWrite(bandLeds[b], intensity);
    }
  }
}

