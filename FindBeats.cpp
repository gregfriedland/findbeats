// Arduino library to find beats in music input from the 7 frequency bands of an MSGEQ7 IC.
// Copyright Greg Friedland 2011 greg.friedland@gmail.com

// TODO:
// - define history size based on time
// - clean up circularbuffer api to get rid of getoffsetindex

#include "FindBeats.h"

#define MAX(x,y) ((x) > (y) ? (x) : (y))

#define PRINT_FPS 1
#define DEBUG 0
#define PLOT 0                    // send data to serial out for processing sketch to plot; DEBUG must be off
#define MOVING_AVG_WINDOW_SIZE 5  // how many samples to use in the moving average
#define MIN_CUTOFF_DIFF 50        // the min diff between the energy and the cutoff


// timing with fastMedian()/fastMad() 3 channels: 541fps
//         with median()/mad() 3 channels: 480fps
//         without either; 3 channels: 740fps
//         without either; 1 channel: 713fps
//         without either; 1 channel, no read spectrum: 111,000fps
//         without either; 1 channel, no delay() in reading spectrum: 3,000fps
//         without either; 1 channel, refactored delay() in reading spectrum: 24,000fps
//         with median/mad; 3 channel, refactored delay() in reading spectrum: 560fps
//         with median/mad; 3 channel, no read spectrum: 6715fps
//         with median/mad; 3 channel, fixed read spectrum: 680fps


// create a temporary array storing the # of times each value was found then find the middle value
// won't work for floats/doubles!
// e.g. 0:1 1:0 2:3 3:0 4:1 (median=2)
// note: is not correct if number of values is zero and averaging is necessary; but good enough for these purposes
static unsigned int fastMedian(unsigned int *a, unsigned int n, unsigned int maxVal) {
  //Serial.println("gothere1");

  // create the temp array
  unsigned int *counts = (unsigned int*) malloc(sizeof(unsigned int) * (maxVal+1));
  memset(counts, 0, sizeof(unsigned int) * (maxVal+1));
  //Serial.println("gothere2");
  
  for (int i=0; i<n; i++) {
    counts[a[i]]++;
  }
  
  // find the median
  //Serial.println("gothere3");
  unsigned int count = 0, currVal = 0, currValCount = 0;
  while (count < (n+1)/2) {
//    Serial.print("count="); Serial.print(count); Serial.print(" currVal="); Serial.print(currVal);
//    Serial.print(" currValCount="); Serial.print(currValCount); Serial.print(" counts[currVal]="); Serial.println(counts[currVal]);
    
    currValCount++;
    count++;
    if (currValCount > counts[currVal]) {
      // find the next nonzero count value
      currVal++;
      while(counts[currVal] == 0) currVal++;
      currValCount = 1;
//      Serial.println("Moving to the next val");
    }
  }
  
//  Serial.print("count="); Serial.print(count); Serial.print(" currVal="); Serial.print(currVal);
//  Serial.print(" currValCount="); Serial.print(currValCount); Serial.println();
  
  free(counts);
  //Serial.println("gothere4");

  return currVal;
}

static unsigned int fastMad(unsigned int *a, unsigned int n, unsigned int median) {
  //Serial.println("gothere2.1");
  unsigned int maxVal = 0;
  unsigned int *diff = (unsigned int*)malloc(sizeof(unsigned int)*n);
  for (int i=0; i<n; i++) {
    diff[i] = abs(a[i] - median);
    maxVal = MAX(maxVal, diff[i]);
  //Serial.println("gothere2.1");
  }
  
  unsigned int madVal = fastMedian(diff, n, maxVal);
  
  free(diff);
  return madVal;
  
}

FindBeats::FindBeats(boolean bandsUsed[NUM_BANDS], uint16_t updatePeriod) {
  _updatePeriod = updatePeriod;
  _lastUpdate = 0;
  uint16_t updatesPerSec = 1000 / updatePeriod;
  
  for (byte b=0; b<NUM_BANDS; b++) {
    _bandsUsed[b] = bandsUsed[b];
    if (!bandsUsed[b]) continue;

    // keep up to 1/2s of history
    _energies[b].init(updatesPerSec/2);
    _meanEnergies[b].init(updatesPerSec/2);
  }
}


// Find beats by looking for signals above a certain number of MADs from the median;
// sensitivity defines the number of MADs and goes from 0-20
void FindBeats::calcBeats(byte sensitivity) {
  byte scale = 20 - sensitivity;
  uint32_t currentTime = millis();
  
  for (byte b=0; b<NUM_BANDS; b++) {
    if (!_bandsUsed[b]) continue;

    // get the moving avg of the energy
    EnergyVal meanE = _energies[b].mean(_energies[b].getOffsetIndex(-MOVING_AVG_WINDOW_SIZE), _energies[b].getIndex());
    _meanEnergies[b].append(meanE);
    
    // use the median and the MAD (median absoulate deviation) to compute the cutoff
    // since these are robust metrics
    EnergyVal medianE = _meanEnergies[b].median();
    EnergyVal madE = _meanEnergies[b].mad(medianE);
//    EnergyVal medianE = fastMedian(_meanEnergies[b].getArray(), _meanEnergies[b].getCapacity(), 1024);
//    EnergyVal madE = fastMad(_meanEnergies[b].getArray(), _meanEnergies[b].getCapacity(), medianE);
    
    EnergyVal cutoff = medianE + scale * madE;

    // the energy must be at least a small amount above the cutoff to be considered an onset
    boolean onset = meanE - cutoff >= MIN_CUTOFF_DIFF;
    
    if (currentTime - _lastBeatTime[b] <= _beatHoldTime) {
      // hold the beat
      _beats[b] = true;
    } else if (currentTime - _lastBeatTime[b] < _beatWaitTime) {
      // haven't waited long enough for the next beat
      _beats[b] = false;
    } else if (onset) {
      // beginning of a new beat
      _beats[b] = true;
      _lastBeatTime[b] = currentTime;
    }
    

#if PLOT
    // log to processing for plotting
    // colors: white, red, green, blue, yellow, cyan, magenta
    Serial.write(byte(_beats[b])*40);                   // white
    Serial.write(byte(onset)*20);                       // red
    Serial.write(map(constrain(meanE,0,1023),0,1024,11,255));                // green
    Serial.write(map(constrain(cutoff,0,1023),0,1024,11,255));      // blue
    Serial.write(map(medianE,0,1024,11,255));          // yellow
    Serial.write(map(madE,0,1024,11,255));          // cyan
#endif
  }
#if PLOT
  Serial.write(10);
#endif
}

static byte readSpectrumBand = 0;
static uint32_t lastStrobeHighTime = 0, lastStrobeLowTime = 0;
static bool strobedLow = false;
// read the next spectrum intensity data from the MSGEQ7
// steps: strobe->LOW, delay 36us, readAnalog(), strobe->HIGH, delay 72us, ...
void FindBeats::readSpectrum() {
  if (readSpectrumBand == NUM_BANDS) return;
  //Serial.print(readSpectrumBand); Serial.println(":1");
  
  // checkpoint to make sure we wait between strobes
  if (micros() - lastStrobeHighTime < 72) return;
  //Serial.print(readSpectrumBand); Serial.println(":2");

  //if (!settled) {
    if (!strobedLow) {
      digitalWrite(_strobePin, LOW); 
      //Serial.print(readSpectrumBand); Serial.println(":3");
      lastStrobeLowTime = micros();
      strobedLow = true;
    }
    
    // checkpoint to make sure we allow the output to settle
    if (micros() - lastStrobeLowTime < 36) return;
    //settled = true;
    //Serial.print(readSpectrumBand); Serial.println(":4");
    //delayMicroseconds(36);
  //}

  EnergyVal e = analogRead(_analogPin);
  e = constrain(e, 0, 1023);
  if (_bandsUsed[readSpectrumBand]) {
    _energies[readSpectrumBand].append(e);
  }
  
  digitalWrite(_strobePin, HIGH);
  //Serial.print(readSpectrumBand); Serial.println(":5");
  lastStrobeHighTime = micros();
  //delayMicroseconds(100); // between strobes
  
  readSpectrumBand++;
  strobedLow = false;
}


// store beat information
void FindBeats::updateBeats(byte sensitivity) {  
  readSpectrum();
  if (readSpectrumBand != NUM_BANDS) return;
  
  // only update if enough time has passed
  uint32_t currentTime = millis();
  if (!DEBUG && currentTime - _lastUpdate < _updatePeriod) return;
  _lastUpdate = currentTime;
  
  calcBeats(sensitivity);
  readSpectrumBand = 0;

#if PRINT_FPS
  // print fps
  static uint32_t loops = 0;
  static uint32_t lastTime = 0;
  loops++;
  if (currentTime - lastTime >= 3000) {
    Serial.print("FindBeats: "); Serial.print(float(loops) / (currentTime - lastTime) * 1000); Serial.println(" fps");
    lastTime = currentTime;
    loops = 0;
  }
#endif
}


void FindBeats::init(byte analogPin, byte strobePin, byte resetPin) {
  _analogPin = analogPin;
  _strobePin = strobePin;
  _resetPin = resetPin;

  pinMode(_analogPin, INPUT);
  pinMode(_strobePin, OUTPUT);
  pinMode(_resetPin, OUTPUT);
  //analogReference(DEFAULT); // XXX what effect does this have
  
  digitalWrite(_resetPin, LOW);
  digitalWrite(_strobePin, HIGH);
  
  // start the chip sending us data
  digitalWrite(_resetPin, HIGH);
  delayMicroseconds(1); 
  digitalWrite(_resetPin, LOW);
  delayMicroseconds(100); // wait after reset before strobing
}


// get the intensity of the beat as it decays from the max value over the onset hold time
byte FindBeats::getBeatIntensity(byte band) {
  uint32_t currentTime = millis();
  return constrain(map(currentTime - _lastBeatTime[band], 0, _beatHoldTime, 255, 0), 0, 255);
}