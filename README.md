FindBeats: an Arduino library for detecting beats in music
==========================================================

This library will find beats present in different frequencies of an audio channel. 
It relies on the [MSGEQ7 IC](https://www.sparkfun.com/datasheets/Components/General/MSGEQ7.pdf) (which is, for example, used in the [Spectrum Shield from Sparkfun](https://www.sparkfun.com/products/10306)) to find the 'energy' level of the audio signal at different frequencies. 
For each frequency band, sudden changes in these energies are detected by the library as beats when they exceed a cutoff. 
This cutoff adapts to increasing average energy and increased variance in the energy since the ear hears beats in contrast 
to the level of other sounds happening at that instant.

Works best when the audio volume is about 50-75% of max.

I've been using this library almost exclusively on the Arduino Due, although I did get it running on the Uno a while back. 
Let me know if you run into issues on other platforms.

Algorithm details (feel free to skip this if you don't care :))
-----------------
1. Smooth out the energy level at each frequency band a little bit by using a moving average over a small window (5 samples)
2. Find the baseline level for each frequency band by calculating the median of this average energy over the recent history (1/2 second). The median is used instead of the mean because it is less sensitive to outliers (i.e. it is more robust).
3. Calculate a measure describing the amplitude of fluctuations in each frequency band over the recent history. The measure used is the [median absolute deviation (MAD)](http://en.wikipedia.org/wiki/Median_absolute_deviation), which is similar in concept to the standard deviation but is more robust to outliers.
4. Find a cutoff for determining if the start of a beat has occurred. A beat is only triggered if it's been at least the beat wait time since the last beat. The cutoff is calculated as `medianEnergy + madEnergy * (20-sensitivity)`.

Parameters
----------
* Beat update period - Upper bound to how often in milliseconds to update the beat information
* Sensitivity - A number between 1 and 20 describing how sensitive the algorithm will be to small or large changes in the audio levels. Higher values mean a lower threshold for deciding whether a beat has occured.
* Beat hold time - How long in milliseconds to 'hold' a beat after it has been triggered.
* Beat wait time - How long to wait after a beat started before allowing another beat to trigger.

API
---
    // Constructor
    FindBeats(boolean bandsUsed[NUM_BANDS], uint16_t updatePeriod);

    // set the pins to use for the MSGEQ7
    void init(byte analogPin, byte strobePin, byte resetPin);

    // how long to hold the beat (in ms)
    void setBeatHoldTime(uint16_t beatHoldTime);
    uint16_t getBeatHoldTime();

    // how long to wait before another beat is allowed (in ms)
    void setBeatWaitTime(uint16_t beatWaitTime);
    uint16_t getBeatWaitTime();

    // when did the start of the last beat occur
    uint32_t getLastBeatTime(byte band);

    // is a beat happening now
    boolean isBeat(byte band);

    // how strong is the beat; 255 is when it starts and 0 is when the hold time is over
    byte getBeatIntensity(byte band);
  
    // run the beat calculations; call this as often as possible at least 1500 times per second but preferably 3000 times
	// it does not use much processor time
    void updateBeats(byte sensitivity);

Example sketch
--------------
	// This sketch flashes leds 13, 12, and 11 in time with the beat on the spectrum shield
	#include <FindBeats.h>
	
	// Pins used by MSGEQ7
	#define ANALOG_PIN A0
	#define RESET_PIN 5
	#define STROBE_PIN 4
	
	#define BEAT_HOLD_TIME 250    // how long to hold the beat for
	#define BEAT_WAIT_TIME 350    // how many ms to wait before allowing another beat                                                                                          
	#define SENSITIVITY 17        // how sensitive to be when looking for beats; higher is more sensitive
	#define UPDATE_PERIOD 5       // the most often time interval that beats are updated: 5ms is a good number; 20 is too high; lower uses more RAM
	
	// only find beats on 3 bands b/c it's already a lot of info and it speed things up
	byte bandLeds[] = {0,13,0,11,0,9,0};
	boolean bandsUsed[] = {0,1,0,1,0,1,0}; // which bands are in use
	FindBeats findBeats(bandsUsed, UPDATE_PERIOD);
	
	void setup() {
	  findBeats.setBeatWaitTime(BEAT_WAIT_TIME);
	  findBeats.setBeatHoldTime(BEAT_HOLD_TIME);
	  findBeats.init(ANALOG_PIN, STROBE_PIN, RESET_PIN);
	}
	
	void loop() {
	  findBeats.updateBeats(SENSITIVITY);
	
	  for (byte b=0; b<NUM_BANDS; ++b) {
		if (bandsUsed[b] && findBeats.isBeat(b)) {
		  byte intensity = findBeats.getBeatIntensity(b);
		  analogWrite(bandLeds[b], intensity);
		}
	  }
	}
