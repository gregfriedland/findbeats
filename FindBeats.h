// Arduino library to find beats in music input from the 7 frequency bands of an MSGEQ7 IC.
// Copyright Greg Friedland 2011 greg.friedland@gmail.com

#ifndef FINDBEATS_H
#define FINDBEATS_H

#include <Arduino.h>
#include "CircularBuffer.h"

#define NUM_BANDS 7

typedef int EnergyVal;

class FindBeats {
 public:

  // constructor where bandsUsed is a boolean array describing which bands to run calculations on
  FindBeats(boolean bandsUsed[NUM_BANDS], uint16_t updatePeriod);

  // set the pins to use for the MSGEQ7
  void init(byte analogPin, byte strobePin, byte resetPin);

  // how long to hold the beat (in ms)
  void setBeatHoldTime(uint16_t beatHoldTime) { _beatHoldTime = beatHoldTime; }
  uint16_t getBeatHoldTime() { return _beatHoldTime; }

  // how long to wait before another beat is allowed (in ms)
  void setBeatWaitTime(uint16_t beatWaitTime) { _beatWaitTime = beatWaitTime; }
  uint16_t getBeatWaitTime() { return _beatWaitTime; }

  // when did the start of the last beat occur
  uint32_t getLastBeatTime(byte band) { return _lastBeatTime[band]; }

  // is a beat happening now
  boolean isBeat(byte band) { return _beats[band]; }

  // how strong is the beat where 255 is when it starts and 0 is when the hold time is over
  byte getBeatIntensity(byte band);
  
  // run the beat calculations
  void updateBeats(byte sensitivity);

 private:
  void calcBeats(byte sensitivity);
  void readSpectrum();

  uint16_t _updatePeriod;
  uint32_t _lastUpdate;
  boolean _bandsUsed[NUM_BANDS];
  CircularBuffer<EnergyVal> _energies[NUM_BANDS]; // storage of the spectrum energies
  CircularBuffer<EnergyVal> _meanEnergies[NUM_BANDS]; // storage of the spectrum energies

  uint32_t _lastBeatTime[NUM_BANDS]; // stores when the last onset was seen
  boolean _beats[NUM_BANDS]; // are we on a beat now?

  byte _analogPin, _strobePin, _resetPin; 
  uint16_t _beatHoldTime, _beatWaitTime;
};
#endif
